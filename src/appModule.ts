import { Module } from '@nestjs/common';
import { AppController } from './appController';

@Module({
  controllers: [AppController],
})
export class AppModule {}
